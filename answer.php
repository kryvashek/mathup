<?php

require_once( 'include/pageparts.php' );
session_start();

if( !seanceCheck() )
	authorizeGo();

$doc = getGet( 'file' );
$dir = getGet( 'section' );
$condition = "${$dir}/$doc";
$answerfile = answerFile( $dir, $doc );

if( checkPost( 'submitanswer' ) )
	{
	$answer = stripslashes( getPost( 'answerarea' ) );
	$result = saveAnswer( $answerfile, $answer );
	
	if( NULL === $result )
		$content = "<div class='tinymced text-container'>Нет информации или файла для сохранения.</div>";
		
	else if( FALSE === $result )
		$content = "<div class='tinymced text-container'>Ошибка при сохранении решения.</div>";
		
	else
		$content = "<div class='answer-container'>$answer</div><div class='tinymced text-container'>Решение успешно сохранено!</div>";
	}
	
else if( TRUE === is_readable( $condition ) )
	{
	if( TRUE === is_readable( $answerfile ) )
		$answer = file_get_contents( $answerfile );

	else
		$answer = '';

	$content = answerForm( $condition, $answer );
	}
	
else
	redirect( $MAIN_PAGE, 5, "<div class='tinymced text-container'>Запрошенный файл отсутствует или не может быть прочтён.</div>" );

echo page( head( TRUE ), body( TRUE, $content ) );

?>
