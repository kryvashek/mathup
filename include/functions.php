<?php

require_once( 'variables.php' );
require_once( 'pageparts.php' );

///=========================

function showAllErrors()
	{
	ini_set( 'display_errors', 1 );
	error_reporting( E_ALL );
	return;
	}

///=========================

function showDefaultErrors()
	{
	ini_set( 'display_errors', 1 );
	error_reporting( E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED );
	return;
	}

///=========================

function hideErrors()
	{
	ini_set( 'display_errors', 0 );
	error_reporting( E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED );
	return;
	}

///=========================

function checkPost( $name, $value = '' )
	{
	if( $value == '' )
		$value = $name;
	return isset( $_POST[ $name ] ) && $_POST[ $name ] === $value;
	}

///=========================

function getGet( $name )
	{
	if( isset( $_GET[ $name ] ) )
		return $_GET[ $name ];
	else
		return NULL;
	}

///=========================

function getPost( $name )
	{
	if( isset( $_POST[ $name ] ) )
		return $_POST[ $name ];
	else
		return NULL;
	}

///=========================

function getSession( $name )
	{
	if( isset( $_SESSION[ $name ] ) )
		return $_SESSION[ $name ];
	else
		return NULL;
	}

///=========================

function getServer( $name )
	{
	if( isset( $_SERVER[ $name ] ) )
		return $_SERVER[ $name ];
	else
		return NULL;
	}

///=========================

function redirect( $new_page, $wait_for = 0, $message = NULL )
	{
	header( "Refresh:$wait_for; url=$new_page" );
	
	if( NULL !== $message )
		echo page( head( FALSE ), body( seanceCheck(), $message ) );

  	exit;
	}

///=========================

function siteRoot()
	{
	return getServer( 'SERVER_NAME' );
	}

///=========================

function currentPage()
	{
	if(	NULL === ( $current = getServer( 'SCRIPT_NAME' ) ) )
		return NULL;
	else
		return basename( $current );
	}

///=========================

function authorizeGo( $current = NULL )
	{
	if( NULL === $current )
		$current = currentPage();

	$_SESSION[ 'prelogin' ] = $current;
	redirect( 'authorize.php' );
	}

///=========================

function authorizeReturn()
	{
	global $AUTHORIZE_TIME;
	global $MAIN_PAGE;

	if( NULL === ( $target = getSession( 'prelogin' ) ) )
		$target = $MAIN_PAGE;
	else
		unset( $_SESSION[ 'prelogin' ] );
		
	redirect( $target, $AUTHORIZE_TIME, "<div class='text-container'>Вы успешно вошли в систему!</div>" );
	}

///=========================

function startPageRoutine()
	{
	//showAllErrors();
	return session_start() && connectDB();
	}

///=========================

function logout()
	{
	if( FALSE === dbAsk( 'DELETE FROM seances WHERE sid=?', array( 1 => session_id() ) ) )
		errorAdd( 'Ошибка удаления сеанса из списка.' );
		
	session_unset();

	/*if( ini_get( 'session.use_cookies' ) )
		{
    	$prms = session_get_cookie_params();
    	setcookie( session_name(), '', time() - 42000, $prms[ 'path' ], $prms[ 'domain' ], $prms[ 'secure' ], $prms[ 'httponly' ] );
		}*/
	
	if( !session_destroy() )
		errorAdd( 'Ошибка завершения сеанса.' );

	return errorAvoided();
	}

///=========================

function answerFile( $section, $document )
	{
	global $ANS;
	global $SALT;
	
	if( isset( $_SESSION[ 'userway' ] ) )
		return "${_SESSION[ 'userway' ]}/$ANS/$section" . hash( 'md5', $section . $document . $SALT[ 'ANSWER' ] );
	
	else
		return NULL;
	//return "$ANS/$section" . hash( 'md5', $section . $document . $SALT[ 'ANSWER' ] );
	}

///=========================

function saveAnswer( $file, $answer )
	{
	if( 0 == strlen( $file ) || 0 == strlen( $answer ) )
		return NULL;
	
	return ( NULL !== ( $resource = fopen( $file, "wb" ) ) &&
			0 < fwrite( $resource, $answer ) &&
			TRUE === fclose( $resource ) );
	}

///=========================

function mess( $str )
	{
	return str_rot13( base64_decode( str_rot13( base64_encode( $str ) ) ) );
	}

///=========================

function newSalt()
	{
	$cstrong = TRUE;
	return openssl_random_pseudo_bytes( 24, $cstrong );
	}

///=========================

function passHash( $pass, $salt )
	{
	global $SALT;
	// make sure that $salt is long and variable enough!
	// and make sure password has at least 6 letters!
	return hash( 'sha256', $salt . mess( $pass ) . $SALT[ 'PASSWORD' ], TRUE );
	}

///=========================

function errorClear()
	{
	global $ERROR;
	$ERROR = NULL;
	}

///=========================

function errorSet( $text )
	{
	global $ERROR;
	$ERROR = $text;
	}

///=========================

function errorAdd( $text )
	{
	global $ERROR;

	if( !is_array( $ERROR ) )
		$ERROR = NULL;

	$ERROR[] = $text;
	}

///=========================

function errorGlue( $gluer = '<br />' )
	{
	global $ERROR;
	
	if( is_array( $ERROR ) && 0 < count( $ERROR ) )
		return implode( $ERROR, $gluer );
	else
		return $ERROR;
	}

///=========================

function errorAvoided() // call it with @ if $err can be undefined
	{
	global $ERROR;
	
	return $ERROR === NULL;
	}

///=========================

function dbConnect()
	{
	if( ( isset( $_SERVER[ 'database' ] )  && NULL !== $_SERVER[ 'database' ] ) || FALSE !== ( $_SERVER[ 'database' ] = new SQLite3( 'data/base.sqlite' ) ) )
		return TRUE;
	
	errorAdd( 'Ошибка подключения к базе данных' );
	return FALSE;
	}

///=========================

function getSqliteArgType( $arg )
	{
	global $SQLITE_TYPES;
	$type = gettype( $arg );
	
	if( isset( $SQLITE_TYPES[ $type ] ) )
		return $SQLITE_TYPES[ $type ];
	else
		return NULL;
	}

///=========================

function dbAsk( $query_text, $query_values = NULL )
	{
	if( !dbConnect() )
		{	
		errorAdd( 'Невозможно выполнить обращение к базе данных' );
		return FALSE;
		}

	$statement = $_SERVER[ 'database' ]->prepare( $query_text );
	
	if( NULL !== $query_values && is_array( $query_values ) )
		foreach( $query_values as $key => $data )
			{
			if( isset( $data[ 'type' ] ) )
				$type = $data[ 'type' ];
			else
				$type = getSqliteArgType( $data );
			
			if( isset( $data[ 'value' ] ) )
				$value = $data[ 'value' ];
			else
				$value = $_SERVER[ 'database' ]->escapeString( $data );
			
			$statement->bindValue( $key, $value, $type );
			}
	
	return $statement->execute();
	}

///=========================

function dbMakeSafe( $string )
	{
	if( dbConnect() )
		return $_SERVER[ 'database' ]->escapeString( $value );

	errorAdd( 'Невозможно выполнить обращение к базе данных' );
	return FALSE;
	}

///=========================

function dbLastRow()
	{
	if( dbConnect() )
		return $_SERVER[ 'database' ]->lastInsertRowID();

	errorAdd( 'Невозможно выполнить обращение к базе данных' );
	return FALSE;
	}

///=========================

function checkEmail( $candidate, $register = TRUE )
	{
	global $EMAIL_PATTERN;
	global $EMAIL_MINLEN;

	if( strlen( $candidate ) < $EMAIL_MINLEN )
		errorAdd( "Указанный электронный адрес слишком короткий (меньше $EMAIL_MINLEN символов)." );

	if( 1 !== preg_match( $EMAIL_PATTERN, $candidate ) )
		errorAdd( 'Недопустимый формат электронного адреса.' );
	
	if( errorAvoided() && $register ) 
		{
		if( FALSE === ( $response = dbAsk( "SELECT COUNT( uid ) FROM users WHERE email=?", array( 1 => $candidate ) ) ) )
			errorAdd( 'Ошибка проверки электронного адреса на уникальность.' );
		
		else if( NULL === ( $count = ( $response->fetchArray( SQLITE3_NUM )[ 0 ] ) ) )
			errorAdd( 'Ошибка проверки электронного адреса на уникальность.' );
		
		else if( 0 < $count )
			errorAdd( 'Указанный электронный адрес уже используется.' );
		}

	return errorAvoided();
	}

///=========================

function checkPassFormat( $pass )
	{
	global $DGTS;
	global $LTRS_LC;
	global $LTRS_UC;
	global $SMBS;
	global $PASS_MINLEN;

	if( strlen( $pass ) < $PASS_MINLEN )
		errorAdd( "Указанный пароль слишком короткий (меньше $PASS_MINLEN символов)." );

	if( 1 !== preg_match( "/[$DGTS]+/", $pass ) )
		errorAdd( 'В пароле не использован ни один цифровой символ.' );

	if( 1 !== preg_match( "/[$LTRS_LC]+/", $pass ) )
		errorAdd( 'В пароле не использован ни один буквенный символ в нижнем регистре.' );

	if( 1 !== preg_match( "/[$LTRS_UC]+/", $pass ) )
		errorAdd( 'В пароле не использован ни один буквенный символ в верхнем регистре.' );

	if( 1 !== preg_match( "/[\Q$SMBS\E]+/", $pass ) )
		errorAdd( "В пароле не использован ни один специальный символ (из набора $SMBS)." );
		
	if( 1 === preg_match( "/[^$DGTS$LTRS_LC\Q$SMBS\E]+/i", $pass ) )
		errorAdd( "В пароле использованы недопустимые символы (допустимые: $DGTS, $LTRS_LC, $LTRS_UC, $SMBS )." );

	return errorAvoided();
	}

///=========================

function checkCredentials( $email, $pass )
	{
	if( FALSE === ( $response = dbAsk( "SELECT uid, pass_hash, salt FROM users WHERE email=? LIMIT 1", array( 1 => $email ) ) ) )
		errorAdd( 'Ошибка проверки регистрации электронного адреса.' );
	
	else
		{
		$data = $response->fetchArray( SQLITE3_ASSOC );
		
		if( NULL === $data[ 'pass_hash' ] || NULL === $data[ 'salt' ] || NULL === $data[ 'uid' ] )
			errorAdd( "Пользователь с таким электронным адресом ($email) не зарегистрирован" );

		else if( strrev( $data[ 'pass_hash' ] ) !== strrev( passHash( $pass, $data[ 'salt' ] ) ) )
			errorAdd( 'Введённый пароль неверен.' );
		
		else if( !seanceValidate( $data[ 'uid' ] ) )
			errorAdd( 'Ошибка валидации сеанса.' );
		}
		
	return errorAvoided();
	}

///=========================

function registerCredentials( $email, $pass, $name = '' )
	{
	global $USR;
	$salt = newSalt();
	$collist = 'email, pass_hash, salt';
	$vallist = '?, ?, ?';
	$values[ 1 ] = $email; // email
	$values[ 2 ] = array( 'type' => SQLITE3_BLOB, 'value' => passHash( $pass, $salt ) ); // password hash
	$values[ 3 ] = array( 'type' => SQLITE3_BLOB, 'value' => $salt ); // salt
	
	if( 0 < strlen( $name ) )
		{
		$collist .= ', name';
		$vallist .= ', ?';
		$values[ 4 ] = $name; // user name
		}

	$query_text = "INSERT INTO users ( $collist ) values ( $vallist )";

	if( FALSE === dbAsk( $query_text, $values ) )
		errorAdd( 'Ошибка при добавлении записи в базу данных.' );
	
	else if( FALSE === ( $userway = userCreateHome( $email, $salt ) ) )
		errorAdd( "Ошибка при создании директории пользователя." );

	return errorAvoided();
	}

///=========================

function sendActivation( $email, $way, $name = '' )
	{
	if( 0 < strlen( $name ) )
		$email = "$name <$email>";
	}

///=========================

function userWay( $email, $salt )
	{
	global $SALT;
	global $USR;
	
	return $USR . '/' . hash( 'ripemd256', $salt . mess( $email ) . $SALT[ 'USERWAY' ], FALSE );
	}

///=========================

function userCreateHome( $email, $salt )
	{
	global $INC;
	global $ANS;

	$userway = userWay( $email, $salt );
	
	if( !mkdir( $userway, 0774 ) || !chmod( $userway, 0774 ) )
		errorAdd( "Ошибка создания каталога пользователя." );
	
	else if( !copy( "$INC/index.php", "$userway/index.php" ) )
		errorAdd( "Ошибка настройки каталога пользователя." );
	
	else if( !mkdir( "$userway/$ANS", 0774 ) || !chmod( "$userway/$ANS", 0774 ) )
		errorAdd( "Ошибка создания каталога ответов пользователя." );
	
	else if( !copy( "$INC/index.php", "$userway/$ANS/index.php" ) )
		errorAdd( "Ошибка настройки каталога ответов пользователя." );
	
	if( errorAvoided() )
		return $userway;
	else
		return FALSE;
	}

///=========================

function seanceHash( $sign, $ip, $salt )
	{
	global $SALT;

	return hash( 'sha256', $salt . mess( $ip ) . $sign . $SALT[ 'SESSION' ], TRUE );
	}

///=========================

function seanceValidate( $uid )
	{
	$values[ 1 ] = session_id(); // sid
	
	if( FALSE === dbAsk( 'DELETE FROM seances WHERE sid=?', $values ) )
		errorAdd( 'Ошибка идентификаторной рафинации перечня сеансов.' );
	
	else
		{
		$_SESSION[ 'sign' ] = newSalt();
		$salt = newSalt();
		$values[ 2 ] = $uid; // fuid
		$values[ 3 ] = time(); // moment
		$values[ 4 ] = array( 'type' => SQLITE3_BLOB, 'value' => $salt ); // salt
		$sign_hash = seanceHash( $_SESSION[ 'sign' ], getServer( 'REMOTE_ADDR' ), $salt );
		$values[ 5 ] = array( 'type' => SQLITE3_BLOB, 'value' => $sign_hash ); // sign_hash
		
		if( FALSE === dbAsk( 'INSERT INTO seances ( sid, fuid, moment, salt, sign_hash ) VALUES ( ?, ?, ?, ?, ? )', $values ) )
			errorAdd( 'Ошибка установления валидности сеанса.' );
		
		else if( FALSE === ( $response = dbAsk( 'SELECT email, salt FROM users WHERE uid=?', array( 1 => $uid ) ) ) )
			errorAdd( 'Ошибка вычисления пути к директории пользователя.' );
		
		else
			{
			$data = $response->fetchArray( SQLITE3_ASSOC );
		
			if( NULL === $data[ 'email' ] || NULL === $data[ 'salt' ] )
				errorAdd( 'В базе данных отсутствуют сведения о пользователе.' );
			
			else
				$_SESSION[ 'userway' ] = userWay( $data[ 'email' ], $data[ 'salt' ] );
			}
			
		}

	return errorAvoided();
	}

///=========================

function seanceCleanup()
	{
	global $SEANCE_DURATION;

	if( FALSE === dbAsk( 'DELETE FROM seances WHERE moment < ?', array( 1 => ( time() - $SEANCE_DURATION ) ) ) )
		errorAdd( 'Ошибка темпоральной рафинации перечня сеансов.' );
		
	return errorAvoided();
	}

///=========================

function seanceCheck()
	{
	if( !seanceCleanup() )
		errorAdd( 'Ошибка при подготовке базы данных.' );
	
	else if( FALSE === ( $rspns = dbAsk( 'SELECT sign_hash, salt FROM seances WHERE sid=? LIMIT 1', array( 1 => session_id() ) ) ) )
		errorAdd( 'Ошибка проверки валидности сеанса.' );
		
	else
		{
		$data = $rspns->fetchArray( SQLITE3_ASSOC );
		
		if( NULL === $data[ 'sign_hash' ] || NULL === $data[ 'salt' ] )
			errorAdd( 'В базе данных отсутствуют сведения о сеансе.' );
		
		else if( strrev( $data[ 'sign_hash' ] ) !== strrev( seanceHash( getSession( 'sign' ), getServer( 'REMOTE_ADDR' ), $data[ 'salt' ] ) ) )
			errorAdd( 'Текущий сеанс не валиден.' );
		}

	return errorAvoided();
	}

?>
