<?php

require_once( 'variables.php' );
require_once( 'functions.php' );

///=========================

function page( $head, $body )
	{
	return "<!DOCTYPE html>
			<html>
				<head>
					$head
				</head>
				<body>
					$body
				</body>
			</html>";
	}

///=========================

function head( $math = FALSE )
	{
	global $MTC;
	global $INC;

	if( TRUE === $math )
		$math = headMath();
	else
		$math = '';

	return	"<meta http-equiv='content-Type' content='application/x-www-form-urlencoded;charset=utf-8' />
		<link href='$MTC/favicon.ico' rel='shortcut icon' />
		<link href='$INC/style/style.css' rel='stylesheet' />
		$math
		<title>Онлайн-школа</title>";
	}

///=========================

function headMath()
	{
	global $TMJ;
	global $TMC;
	global $INC;
	return "<script type='text/javascript' src='$TMJ/plugins/asciimath/js/ASCIIMathMLwFallback.js'></script>
		<script type='text/javascript'>var AMTcgiloc = 'http://www.imathas.com/cgi-bin/mimetex.cgi';</script>
		<script type='text/javascript' src='$TMJ/tiny_mce.js' ></script >
		<script type='text/javascript' >
		tinyMCE.init({
			mode : 'textareas',
			theme : 'advanced',
			theme_advanced_buttons1 : 'fontselect,fontsizeselect,formatselect,bold,italic,underline,strikethrough,separator,sub,sup,separator,cut,copy,paste,undo,redo',
			theme_advanced_buttons2 : 'justifyleft,justifycenter,justifyright,justifyfull,separator,numlist,bullist,outdent,indent,separator,forecolor,backcolor,separator,hr,link,unlink,image,media,table,code,separator,asciimath,asciimathcharmap,asciisvg',
			theme_advanced_buttons3 : '',
			theme_advanced_fonts : 'Arial=arial,helvetica,sans-serif,Courier New=courier new,courier,monospace,Georgia=georgia,times new roman,times,serif,Tahoma=tahoma,arial,helvetica,sans-serif,Times=times new roman,times,serif,Verdana=verdana,arial,helvetica,sans-serif',
			theme_advanced_toolbar_location : 'top',
			theme_advanced_toolbar_align : 'left',
			theme_advanced_statusbar_location : 'bottom',
			plugins : 'asciimath,asciisvg,table,inlinepopups,media',
			AScgiloc : '$TMC/php/svgimg.php',
			ASdloc : '$TMJ/plugins/asciisvg/js/d.svg',
			content_css : '$INC/style/style.css'
		});
		</script>";
	}

///=========================

function body( $authorized, $content )
	{
	return "<div id='allpage'>".
				pageHeader().
				divRow( divCell( mainMenu( $authorized ) ) ).
				divRow( divCell( $content, 'payload' ) ).
			'</div>';
	}

///=========================

function pageHeader()
	{
	global $MTC;
	return "<div id='header'>
				<span id='logo'><img  width='105' height='107' alt='' src='$MTC/logo.png' /></span>
				<span id='caption'><h1>БФУ им. И. Канта.<br />Онлайн-школа математики и&nbsp;информатики.</h1></span>
			</div>";
	}

///=========================

function mainMenu( $authorized )
	{
	global $MAIN_MENU;
	
	$menu = $MAIN_MENU;
	
	if( TRUE == $authorized )
		$menu[] = array('name'	=> 'Выход',
						'action'=> 'logout.php' );
	else
		$menu[] = array('name'	=> 'Вход / Регистрация',
						'action'=> 'authorize.php' );

	return menuBar( $menu, 'mm' );
	}

///=========================

function menuBar( $buttons_array, $bar_name = 'mnbr' )
	{
	$buttons = '';

	foreach( $buttons_array as $button_index => $button_data )
		$buttons .= menuButton( "${bar_name}btn$button_index", $button_data[ 'name' ], $button_data[ 'action' ] );
		
	return "<div class='mnbr'>$buttons</div>";
	}

///=========================

function menuButton( $name, $value, $action = '' )
	{
	if( '' == $action )
		$action = currentPage();		

	$button = "<button name='$name' value='$name'>$value</button>";

	return "<form class='mnbtn' method='post' action='$action'>$button</form>";
	}

///=========================

function answerForm( $condition, $text = '' )
	{
	if( '' == $text )
		$text = "<p>Изложи в этом текстовом поле своё решение и отправь его на проверку, нажав кнопку \"Отправить на проверку\".<br />Вот пример формулы: <span class='AM'>`y=sum_(n=0)^(oo)int_1^(oo)log_x^(-n)([x]!)dx`</span><br />Мы с нетерпением ждём твоих решений!</p>";

	return "<div align='center' class='container'>
		<object type='application/pdf' border='1' width='100%' height='275' data='$condition'></object>
	</div>
	<div align='center' class='container tinymced'>
		<form method='post'>
			<textarea name='answerarea' style='width: 100%;' rows='25' >
				$text
			</textarea>
			<button type='submit' name='submitanswer' value='submitanswer'>Отправить на проверку</button>
		</form>
	</div>";
	}

///=========================

function errorBlock()
	{
	return '<div class=\'text-container error\'>' . errorGlue() . '</div>';
	}

///=========================

function divRow( $content, $class = '' )
	{
	return "<div class='row $class'>$content</div>";
	}

///=========================

function divCell( $content, $class = '' )
	{
	return "<div class='cell $class'>$content</div>";
	}

///=========================

function authorizeForm( $type, $email = '', $name = '' )
	{
	if( 0 < strlen( $email ) )
		$email = " value='$email'";
		
	if( 0 < strlen( $name ) )
		$name = " value='$name'";
		
	$ever = divRow( divCell( "<input type='text' name='email' placeholder='Email'$email required/>", 'center' ), 'text-container' ).
			divRow( divCell( "<input type='password' name='pass' placeholder='Пароль' required/>", 'center' ), 'text-container' );
	$enter = divRow( divCell( "<button type='submit' name='enter' value='enter'>Войти</button>", 'center' ) );
	$both = divRow( "<p class='center cell'>Попав сюда впервые, ты можешь легко записаться на занятия в онлайн-школе:</p>", 'text-container' );
	$register = divRow( divCell( "<input type='text' name='name' placeholder='Имя пользователя'$name/>", 'center' ), 'text-container' ).
				divRow( divCell( "<button type='submit' name='signup' value='signup'>Записаться</button>", 'center' ) );		
				
	switch( $type )
		{
		case 'both' :
			break;

		case 'enter' :
			$both = NULL;
			$register = NULL;
			break;
		
		case 'register' :
			$enter = NULL;
			$both = NULL;
			break;

		default :
			errorAdd( 'Неизвестный подвид формы авторизации' );
		}
	
	return "<form method='post'><div class='register-container'>$ever$enter$both$register</div></form>";
	}

?>
