<?php

$SALT = array(
	'SESSION'	=> crc32( 'Vita nostra brevis est' ),
	'PASSWORD'	=> crc32( 'Brevi finietur' ),
	'ANSWER'	=> crc32( 'Venit mors velociter' ),
	'USERWAY'	=> crc32( 'Rapit nos atrociter' ),
	'three'		=> crc32( 'Nemini parcetur' ) );

$SQLITE_TYPES = array(
	'double'	=> SQLITE3_FLOAT,
	'integer'	=> SQLITE3_INTEGER,
	'boolean'	=> SQLITE3_INTEGER,
	'NULL'		=> SQLITE3_NULL,
	'string'	=> SQLITE3_TEXT );

$AUTHORIZE_TIME = 5;// secs to show message user is authorized
$SEANCE_DURATION = 3*60*60; // 3 hours
$DGTS = '0-9';
$LTRS_LC = 'a-z';
$LTRS_UC = 'A-Z';
$SMBS = '.,!?:;=+-_&%$#@~()';
$EMAIL_PATTERN = '/.+@.+\..+/i';
$PASS_MINLEN = 6;
$EMAIL_MINLEN = 5;

$ERROR = NULL;

$MAIN_PAGE = 'index.php';
$SRV = 'http://'.$_SERVER[ 'SERVER_NAME' ];
$ANS = 'answers';
$MTR = 'materials';
$INC = 'include';
$USR = 'users';

$MTC = $MTR.'/common';
$MTM = $MTR.'/math';
$MTI = $MTR.'/it';

$TMC = $INC.'/tinymce';
$TMJ = $TMC.'/jscripts';

$MAIN_MENU = array( array(	'name'		=> 'Проект', 
							'action'	=> 'index.php' ),
							
					array(	'name'		=> 'Математика',
							'action'	=> 'math.php' ),
							
					array(	'name'		=> 'Информатика',
							'action'	=> 'it.php' )/*, 'Выход'*/ );

?>
