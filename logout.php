<?php

require_once( 'include/pageparts.php' );
session_start();

$result = logout();
redirect( $MAIN_PAGE, $AUTHORIZE_TIME, $result ? "<div class='text-container'>Вы вышли из системы.</div>" : errorBlock() );

?>
