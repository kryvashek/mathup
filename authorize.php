<?php

require_once( 'include/pageparts.php' );

session_start();

if( !dbConnect() )
	$content = errorBlock();

else if( checkPost( 'enter' ) )
	{
	$pass = getPost( 'pass' );
	$email = getPost( 'email' );
	
	if( checkCredentials( $email, $pass ) )
		authorizeReturn();
	else
		$content = authorizeForm( 'enter', $email ) . errorBlock();
	}
else if( checkPost( 'signup' ) )
	{
	$pass = getPost( 'pass' );
	$email = getPost( 'email' );
	$name = getPost( 'name' );
	
	if( checkEmail( $email ) && checkPassFormat( $pass ) && registerCredentials( $email, $pass, $name ) )
		redirect( 'authorize.php', $AUTHORIZE_TIME, "<div class='text-container'>Регистрация успешно пройдена!</div>" );
	else
		$content = authorizeForm( 'register', $email, $name ) . errorBlock();
	}
else
	$content = authorizeForm( 'both' );

echo page( head( FALSE ), body( FALSE, $content ) );
?>

