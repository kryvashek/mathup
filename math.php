<?php

require_once( 'include/pageparts.php' );
session_start();

if( !seanceCheck() )
	authorizeGo();

	//$link = $SRV.'/answer.php?section=';
	$link = 'answer.php?section=';
	$content =
	"<table border='1' align='center'>
	<tr>
	<td align='center'>
	Первое занятие
	</td>
	<td><a href='$MTM/1_Parameter_introduction.pptx'>Решение задач с параметрами</a><br />
	<a href='$MTM/1_Parameter_introduction_01.swf'>Визуализация №1</a><br />
	<a href='$MTM/1_Parameter_introduction_02.swf'>Визуализация №2</a><br />
	<a href='$MTM/1_Parameter_introduction_03.swf'>Визуализация №3</a></td>
</tr>
<tr><td align='center'>
	Второе занятие
	</td>
	<td><a href='$MTM/2_Parameter_quadro.pptx'>Квадратные уравнения с параметром</a><br />
	<a href='${link}MTM&file=2_Parameter_quadro_conditions_solutions.pdf'>Задачи и решения</a></td>
</tr>
<tr><td align='center'>
	Третье занятие
	</td>
	<td>
	<a href='${link}MTM&file=3_15_condition.pdf'>Условие задачи 15</a><br />
	<a href='$MTM/3_15_solution.pdf'>Решение задачи 15</a><br />
	<a href='${link}MTM&file=3_16_condition.pdf'>Условие задачи 16</a><br />
	<a href='$MTM/3_16_solution.pdf'>Решение задачи 16</a><br />
	<a href='${link}MTM&file=3_17_condition.pdf'>Условие задачи 17</a><br />
	<a href='$MTM/3_17_solution.pdf'>Решение задачи 17</a><br />
	<a href='${link}MTM&file=3_18_condition.pdf'>Условие задачи 18</a><br />
	<a href='$MTM/3_18_solution.pdf'>Решение задачи 18</a><br />
	<a href='${link}MTM&file=3_19_condition.pdf'>Условие задачи 19</a><br />
	<a href='$MTM/3_19_solution.pdf'>Решение задачи 19</a><br />
	<a href='${link}MTM&file=3_20_condition.pdf'>Условие задачи 20</a><br />
	<a href='$MTM/3_20_animation.swf'>Анимация к задаче 20</a><br />
	<a href='$MTM/3_20_solution.pdf'>Решение задачи 20</a><br />
	<a href='${link}MTM&file=3_21_condition.pdf'>Условие задачи 21</a><br />
	<a href='$MTM/3_21_solution.pdf'>Решение задачи 21</a></td>
</tr>
<tr><td align='center'>
	Четвёртое занятие
	</td>
	<td>
	<a href='$MTM/4_Parameter_USE.pptx'>Задачи с параметром на ЕГЭ</a><br />
	<a href='$MTM/4_Parameter_USE_2014_C5.swf'>Задание C5, 2014 год</a><br />
	<a href='$MTM/4_Parameter_USE_2015_01.swf'>Задание 1, 2015 год</a><br />
	<a href='$MTM/4_Parameter_USE_2015_02.swf'>Задание 2, 2015 год</a><br />
	<a href='$MTM/4_Parameter_USE_2016_01.swf'>Задание 1, 2016 год</a><br />
	<a href='$MTM/4_Parameter_USE_2016_02.swf'>Задание 2, 2016 год</a><br />
	<a href='$MTM/4_Parameter_USE_2016_03.swf'>Задание 3, 2016 год</a><br />
</tr>
<tr>
	<td colspan='2' align='center'>Решения, оформленные в файлах формата PDF или MS Word 97-2007,<br /> отсылайте по адресу: <a href='mailto:?subject=%D0%A0%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B7%D0%B0%D0%B4%D0%B0%D1%87'>vkhudenko@kantiana.ru</a></td>
</tr></table>";


echo page( head( FALSE ), body( TRUE, $content ) );

?>
